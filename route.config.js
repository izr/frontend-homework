module.exports = {
    port: 1234,
    pages: {
        main: '/',
        button: '/button',
        popover: '/popover',
    }
};

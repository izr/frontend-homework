const path = require('path');
const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');

const locations = [
  path.resolve(__dirname, 'frontend'),
  path.resolve(__dirname, 'backend')
];



module.exports = [{
  entry: './frontend/index.js',
  mode: NODE_ENV,
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      { test: /\.js$/, loader: "babel-loader",include: locations },
      {
        test: /\.css$/,
        include: locations,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  },
  devServer: {
    publicPath: "/",
    contentBase: "./public"    
}
},
{
  entry: ['./backend/entry.js'],
  output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'server.js'
  },
  target: 'node',
  mode: NODE_ENV,
  plugins: [
    new webpack.DefinePlugin({
        __dirName: '__dirname',
        NODE_ENV: JSON.stringify(NODE_ENV)
    })],

  module: {
      rules: [
          {
              test: [/\.jsx?$/, /\.js$/],
              include: locations,
              use: { loader: 'babel-loader' }
          }, {
              test: /\.(png|jpg|gif|svg)$/,
              include: locations,
              use: { loader: 'url-loader' }
          }, {
              test: /\.less$/,
              include: locations,
              use: [
                  { loader: 'css-loader'   },
                  { loader: 'less-loader'  }
              ]
          },
      ],
  },
}


];
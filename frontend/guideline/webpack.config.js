const path = require('path')

module.exports = {
  entry: './index.js',
  mode: 'production',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'lib'),
    library: '',
    libraryTarget: 'commonjs'
  },
  externals: {
    classnames: 'classnames',
    react: 'react',
    'react-dom': 'react-dom'
  },
  module: {
    rules: [
      { test: /\.js$/, loader: 'babel-loader' },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  }
}

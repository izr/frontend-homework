import React from 'react'
import './style.css'
import classnames from 'classnames'
import { Route, Link, Switch } from 'react-router-dom'

export default class Navigation extends React.Component {
  render() {
    const { pages } = this.props
    console.log(this.props)
    
     return (
     <div>
        <nav className="nav">
          {pages.map(p => (
            <Link
              key={p.name}
              to={p.path}
              className={classnames(
                'nav__item',
              )}
            >
              {p.name}
            </Link>
          ))}
        </nav>

        <Switch>
          {pages.map(p => (
            <Route exact path={p.path} component={p.component} />
          ))}
        </Switch>
      </div>
    )
  }
}

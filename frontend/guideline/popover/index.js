import React from 'react'
import './style.css'

export default class Popover extends React.Component {
  render() {
    const { show, children } = this.props
    return (
      <div
        className="popover__message"
        style={{ display: show ? 'block' : 'none' }}
      >
        {children}
      </div>
    )
  }
}

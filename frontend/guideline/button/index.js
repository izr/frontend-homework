import React from 'react'
import './style.css'
import classnames from 'classnames'

export default class Button extends React.Component {
  render() {
    const { children, disabled,round, large } = this.props
    return <button disabled={disabled} onClick={this.onClick} className={classnames('btn', round && 'btn--round', large && 'btn--large' )}>{children}</button>
  }

  onClick = e => {
      this.props.onClick && this.props.onClick()
  }
}

import React from 'react'
import { Button }  from 'guideline-test21';


export default class ButtonsPage extends React.Component {
    state = {
      buttonClicked: null,
    }
    render() {
      return (
        <div>
          {this.state.buttonClicked && (
            <p>You clicked {this.state.buttonClicked}</p>
          )}
          <div>
            <Button onClick={() => this.setState({ buttonClicked: 'Button' })}>
              Button
            </Button>
            <Button
              disabled
              onClick={() => this.setState({ buttonClicked: 'disabled' })}
            >
              disabled
            </Button>
  
            <Button
              round
              onClick={() => this.setState({ buttonClicked: 'Round' })}
            >
              Round
            </Button>
            <Button
              large
              onClick={() => this.setState({ buttonClicked: 'Large' })}
            >
              Large
            </Button>
            <Button
              large
              round
              onClick={() => this.setState({ buttonClicked: 'Large & Round' })}
            >
              Large & Round
            </Button>
          </div>
        </div>
      )
    }
  }
  
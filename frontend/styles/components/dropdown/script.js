(function () {
    var containerClass = '.dropdown';
    var containers = document.querySelectorAll(containerClass);

    function open(menu, dropdown) {
        menu.style.display = 'block';
        dropdown.dataset.open = true;
    }

    function close(menu, dropdown) {
        menu.style.display = 'none';
        dropdown.dataset.open = false;
    }

    Array.from(containers).map(function (container) {
        var button = container.querySelector('.dropdown__button');
        var menu = container.querySelector('.dropdown__menu');
        var menuOptions = container.querySelectorAll('.dropdown__menu-option');
        var dropdownText = container.querySelector('.dropdown__text');

        button.onclick = () => {
            if(container.dataset.open === 'true') {
                close(menu, container);
            } else{
               open(menu, container);
            }
        }
        Array.from(menuOptions).map(function (option) { 
            option.onclick = () => {
                dropdownText.textContent = option.textContent;
                close(menu, container);
            };

        });
    });
})();

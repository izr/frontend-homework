(function () {
    var containerClass = '.modal-container';
    var containers = document.querySelectorAll(containerClass);

    Array.from(containers).map(function (container) {
        var closeButton = container.querySelector('.modal__close-button');
        var openButton = container.querySelector('.open-modal');
        var modal = container.querySelector('.modal');
        var titleElement = container.querySelector('.modal__title');
        var backdrop = container.querySelector('.modal-backdrop');
        var title = container.dataset.modalTitle;

        var close = () => {
            modal.classList.remove('modal--visible');
            modal.classList.add('modal--hidden');
            backdrop.style.display = 'none';
        }



        backdrop.onclick = close;
        closeButton.onclick = close;

        openButton.onclick = () => {
            
            backdrop.style.display = 'block';
            titleElement.textContent = title;
            modal.classList.add('modal--visible');
            modal.classList.remove('modal--hidden');
        }
    });
})();

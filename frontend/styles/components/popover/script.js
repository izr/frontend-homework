(function () {
    var containerClass = '.popover';
    var containers = document.querySelectorAll(containerClass);

    Array.from(containers).map(function (container) {
        var triggerElement = container.querySelector('.popover__trigger');
        var message = container.querySelector('.popover__message');
        var trigger = container.dataset.trigger;

        if(trigger === 'click') {
            triggerElement.onclick = () => {
                message.style.display = message.style.display === 'block' ? 'none' : 'block';
            }
        }
        if(trigger === 'hover') {
            triggerElement.onmouseover = e =>  message.style.display = 'block';
            triggerElement.onmouseout = () =>  message.style.display = 'none';
            //    console.log(e.target);
            
            }        
    });
})();

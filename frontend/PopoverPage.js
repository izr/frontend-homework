import React from 'react'
import { Button, Popover }  from 'guideline-test21';

export default class PopoverPage extends React.Component {
  state = {
    popover1Show: false,
    popover2Show: false,
  }
  render() {
    return (
      <div>
        <div>
          <Button
            onClick={() =>
              this.setState(state => ({ popover1Show: !state.popover1Show }))
            }
          >
            Toggle popover
          </Button>
          <Popover show={this.state.popover1Show}> Popover11111</Popover>
        </div>
        <div>
          <div
            onMouseEnter={() => this.setState({ popover2Show: true })}
            onMouseLeave={() => this.setState({ popover2Show: false })}
            style={{ width: '100px', height: '100px', backgroundColor: 'gray' }}
          />

          <Popover show={this.state.popover2Show}> Hover popover</Popover>
        </div>
      </div>
    )
  }
}

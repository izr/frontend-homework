import React from 'react'
import ButtonsPage from './ButtonsPage'
import PopoverPage from './PopoverPage'

import { Navigation } from 'guideline-test21'
import routing from '../route.config'

const pageByName = {
  button: ButtonsPage ,
  popover: PopoverPage ,
  main:  () => <b>Main page</b>,
}

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Navigation
          pages={Object.entries(routing.pages).map(p => ({
            path: p[1],
            name: p[0],
            component: pageByName[p[0]],
          }))}
        />

        <b>It works!!</b>
      </div>
    )
  }
}

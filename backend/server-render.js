export default (props) => {
    return `<!DOCTYPE html>
        <html>
            <head>
            <base href="/">
            <title>${props.title || 'default'}</title>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width" />
            </head>
            <body>
                <div id="root"></div>
                <script src="bundle.js"></script>
            </body>
        </html>`
}

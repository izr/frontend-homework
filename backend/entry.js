import express from 'express';
import routing from '../route.config.js';
import Layout from './server-render';
import path from 'path';

console.log(__dirname);

const app = express();

app.use(express.static('./public'));
app.listen(routing.port);

const metaInfo = {
    [routing.pages.main]: 'main',
    [routing.pages.button]: 'button',
    [routing.pages.popover]: 'popover',
};

app.get(Object.values(routing.pages), (req, res) => {
    res.send(Layout({ title: metaInfo[req.path] }));
});

/*app.get('*', (req, res) => {
    res.send(Layout({ title: metaInfo[req.path] }));
});*/

console.log(`web server is running on port ${routing.port}`);
